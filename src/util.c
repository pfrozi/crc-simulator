#include "../include/util.h"

void print_Nbits(unsigned long long n, int size, char** out){

    unsigned long long mask = (unsigned long long)1 << (size-1);

    unsigned int k;
    char* a = (char*)malloc(sizeof(char)*size);

    for (k = 0; k < size; k++) {
        if (n & mask)
            a[k] = '1';
        else
            a[k] = '0';

        n <<= 1;
    }

    *out = a;
}

void convert_char_to_64(char* s, unsigned long long** out){

    unsigned int s_size = strlen(s)+1;
    char* a = (char*)malloc( sizeof(char)*s_size );

    memset(a,'\0',sizeof(char)*s_size);
    memcpy(a,s,s_size);

    unsigned int i;

    unsigned int leny = (1+(s_size-1)/sizeof(char));

    unsigned long long* y
        = (unsigned long long*)malloc(sizeof(unsigned long long)*leny);

    memset(y, 0,sizeof(unsigned long long)*leny);

    for(i=0;i<s_size;i++){
        y[i/(unsigned int)BYTE_SIZE] <<= BYTE_SIZE;
        y[i/(unsigned int)BYTE_SIZE] |= a[i];

        printf(">>>>> %d %d %016lx %016lx <<<<<<\n",i,i/(unsigned int)BYTE_SIZE,y[i/BYTE_SIZE], a[i]);
    }
    free(a);
    *out = y;
}

void convert_charhex_to_64(char* s, unsigned long long* out){

    unsigned int s_size = strlen(s);

    *out = 0;
    unsigned int i;
    unsigned int aux;
    for(i=0;i<s_size;i++){

        aux = 0;

        if(s[i]>='0' && s[i]<='9'){

            aux = s[i]-48;

        }else if(s[i]>='A' && s[i]<='F'){

            aux = s[i]-55;
        }

        *out += (unsigned long long)(aux*pow(16,(s_size-1)-i));
    }

}
