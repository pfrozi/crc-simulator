#include <stdio.h>



int main()
{
    //64 bits long - 8 BYTES
    unsigned long long x = 0x91;

    char a[8] = "BSAD";

    unsigned int i;
    unsigned long long y = 0;

    printf("/nchar:");
    for(i=0;i<8;i++){
        y <<= 8;
        y |= a[i];
        printf("\n%03d", a[i]);
        printf("\t-\t%#02x", a[i]);
        printf("\t-\t");
        printf_bin32(a[i]);
    }
    printf("\n\n");

    printf("\nThe POLY(hex)  : %016lx\t",x);
    printf_bin64(x);
    printf("\nThe number(hex): %016lx\t",y);
    printf_bin64(y);

    printf("\n\n");

    int grau=8;
    unsigned long long poly_mask  = (unsigned long long)x << (64-grau);
    unsigned long long poly_mask1 = (unsigned long long)2*2*2*2*2*2*2*2-1 << (64-grau);
    printf("\nMaskPol1: ");
    printf_bin64(poly_mask1);
    printf("\nDividend: ");
    printf_bin64(y);

    printf("\nAND     : ");
    printf_bin64(poly_mask1&y);
    printf("\nMaskPol : ");
    printf_bin64(poly_mask);
    printf("\nXOR     : ");
    printf_bin64(poly_mask^(poly_mask1&y));

    printf("\n\n");

    unsigned int q =0;
    unsigned long long r = 0;
    unsigned long long d,g;
    r=0;
    while(1){

        d=poly_mask1&y;
        if(d>r){
            g=poly_mask;
        }else{
            g=0;
        }
        r=d^g;

        poly_mask1>>=1;


    }

    printf("\n\n");
    return 0;
}
