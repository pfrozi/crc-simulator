#include "../include/crc_module.h"

void crc_encode(
      unsigned long long[] msg
    , unsigned int         msg_size
    , unsigned long long   polynomial
    , unsigned int         polynomial_degree
    , unsigned long long** out){

    unsigned long long fcs     = msg[0];
    unsigned long long mask_b0 = 1;

    mask_b0 <<= MAXIMUM_SIZE-1;

    // fcs must have the same length of polynomial
    fcs >>= MAXIMUM_SIZE - polynomial_degree;

    //Shift msg[0], since msg[0] has been attributed to fcs.
    msg[0] <<= polynomial_degree;

    #if(CRC_MODULE_DEBUG)
    printf("\ncrc_module: crc_encode() - inicial fcs: %llu\n", fcs);
    #endif

    unsigned int k = 0;
    for (k = 0; k < message_size; k++) {

        while(msg[k]){

            if(msg[k]&mask_b0){

                fcs ^= polynomial;
            }

            msg[k] <<= 1;
        }
    }

    #if(CRC_MODULE_DEBUG)
    printf("\ncrc_module: crc_encode() - fcs value(hex): %016lx\n",fcs);
    #endif

    // concatenate the fcs to the tail end of msg


    #if(CRC_MODULE_DEBUG)
    printf("\ncrc_module: crc_encode(): Init\n");
    #endif

    return NULL;
}

int crc_noise(protocol_t* p){

    return 0;
}

int crc_decode(long pol, protocol_t* p){

    return 0;

}
