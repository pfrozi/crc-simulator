#ifndef CRC_MODULE_H
#define CRC_MODULE_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAXIMUM_SIZE        64
#define POLYNOMIAL          0x91

#define SIZE_OF_MSG         4

#define CRC_DECODE_SUCCESS  0
#define CRC_DECODE_FAIL     1

#define CRC_MODULE_DEBUG    1


// Encode message with crc. The function needs a long long representation
// of string message to calculate the FCS. then it's placed at the tail end of message.
//      Parameters:
//          message    - message long array, in hexadecimal representation
//          polynomial - polynomial
//          out        - encode message concatenated with the FCS
void crc_encode(
      unsigned long long[] msg
    , unsigned int         msg_size
    , unsigned long long   polynomial
    , unsigned int         polynomial_degree
    , unsigned long long** out);

// Put a noise into a message
int         crc_noise(protocol_t* p);
// Decode message
int         crc_decode(long pol, protocol_t* p);

#endif
