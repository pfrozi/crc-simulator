all: crc_dc

crc_dc: libutil
	@gcc -g -o bin/crc_dc src/main.c -Llib -lutil -lm

libutil: crc_module.o util.o
	@ar crs lib/libutil.a bin/crc_module.o bin/util.o

crc_module.o: src/crc_module.c
	@gcc -c src/crc_module.c -o bin/crc_module.o

util.o: src/util.c
	@gcc -c src/util.c -o bin/util.o

clean:
	@rm -rf bin/*.o
	@rm -rf bin/crc_dc
	@rm -rf lib/*.a
