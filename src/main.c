#include "../include/main.h"

void help(){

    printf("\nTrabalho de Comunicacao de Dados");
    printf("\nCodificador / Decodificador CRC");
    printf("\n");
    printf("\nCRC");
    printf("\n\t-m\t, Mensagem (texto ascii)");
    printf("\n\t-e\t, Probabilidade que um bit seja invertido");
    printf("\n\t-d\t, Grau do polinomio gerador");
    printf("\n\t-P\t, Polinomio gerador no formato HEXA (Ex.: A91, max 64 bits)");
    printf("\n\t-t\t, Realizar teste automatico");
    printf("\n");
    printf("\n");
}


int main(int argc, char **argv) {

    #if(MAIN_DEBUG)
    printf("main: main() init\n");
    #endif

    bool play_test = false;

    char*  message;
    unsigned long long* llmessage;
    unsigned long long* encoded_message;
    int    degree;  // polynomial degree
    float  e_rate;

    char*  polynomial;
    unsigned long long polynomial64;
    char* polynomial_bin;

    if (argc < ARGS_MAX || (argc==1 && !(strcmp(argv[1],ARGS_HELP)==0) )) {

        printf("%s\n",ERR_ARGS);

        help();
        return (ERROR_INVALID_ARGS);

    } else {

        int i;

        for (i = 1; i < argc; i+=2) {

            if (i + 1 != argc)
            {
                char* arg = argv[i+1];

                if (strcmp(argv[i],ARGS_HELP) == 0) {

                    help();
                    return (RETURN_NOTHING);

                } else if (strcmp(argv[i], ARGS_MSG)==0) {

                    message = arg;

                } else if (strcmp(argv[i], ARGS_ERROR_RATE)==0) {

                    e_rate = atof(arg);

                } else if (strcmp(argv[i], ARGS_POL_DEGREE)==0) {

                    degree = atoi(arg);

                } else if (strcmp(argv[i], ARGS_GEN_POL)==0) {

                    polynomial = arg;

                } else if (strcmp(argv[i], ARGS_TEST)==0) {

                    play_test = true;

                }else {

                    printf("%s\n", ERR_ARGS);
                    return (ERROR_INVALID_ARGS);

                }
            }
        }
    }


    if(play_test){

        // TODO:
    }else {
        // get the lng and binary representation of pol
        convert_charhex_to_64(polynomial, &polynomial64);
        print_Nbits(polynomial64, MAXIMUM_SIZE, &polynomial_bin);

        // convert the message to long long array
        convert_char_to_64(message, &llmessage);

        #if(MAIN_DEBUG)
        printf("main_cli: main() - Message: %s\n", message);
        printf("main_cli: main() - Error rate: %f\n", e_rate);
        printf("main_cli: main() - Polynomial Degree: %d\n", degree);
        printf("main_cli: main() - Polynomial(hex): %s\n", polynomial);
        printf("main_cli: main() - Polynomial(lng): %llu\n", polynomial64);
        printf("main_cli: main() - Polynomial(bin): %s\n", polynomial_bin);

        unsigned int k;
        for (k = 0; k < strlen(message)+1; k++) {
            printf("main_cli: main() - Message(hex array)[%d]: %016lx\n", k, message[k]);
        }
        for (k = 0; k < 1+(strlen(message)+1)/BYTE_SIZE; k++) {
            printf("main_cli: main() - Message(lng)[%d]: %016lx\n", k, llmessage[k]);
        }

        #endif

        // encode the message
        crc_encode(llmessage
            , (strlen(message)+1)/BYTE_SIZE
            , polynomial64
            , polynomial_degree
            , &encoded_message);

        #if(MAIN_DEBUG)
        printf("main_cli: main() - Message Encoded:\n");
        unsigned int k;
        for (k = 0; k < 1+(strlen(message)+1)/BYTE_SIZE; k++) {
            printf("\t\t\t[%d]: %016lx\n", k, llmessage[k]);
        }

        #endif
    }

    return (RETURN_SUCCESS);
}
