#ifndef MAIN_H
#define MAIN_H

#include <stdio.h>
#include <stdlib.h>
#include "crc_module.h"
#include "util.h"

/*
    CONSTS
*/

#define MAIN_DEBUG         1


#define RETURN_NOTHING     1
#define ERROR_INVALID_ARGS 2
#define ERROR_INVALID_TYPE 3
#define RETURN_SUCCESS     0

#define ARGS_MAX_CRC       6
#define ARGS_MAX           ARGS_MAX_CRC

#define ARGS_HELP        "-h"  // Return a help text

#define ARGS_MSG         "-m"  // Message that will be sent
#define ARGS_ERROR_RATE  "-e"  // Error rate
#define ARGS_POL_DEGREE  "-d"  // Pol degree
#define ARGS_GEN_POL     "-P"  // Generator polynomial
#define ARGS_TEST        "-t"  // Play a test!


/*
    Text ERRORS
*/
#define ERR_ARGS          "Argumentos invalidos!"
#define ERR_PORT_NUMBER   "Tipos invalidos!"

void help();

#endif
