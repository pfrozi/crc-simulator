#ifndef UTIL_H
#define UTIL_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

#define BYTE_SIZE 8

// Print out a binary representation of [n] number
// To do this, the [size] of binary word is needed.
void print_Nbits(unsigned long long n, int size, char** out);


// Convert a char array to long long array (64bits*N)
void convert_char_to_64(char* array, unsigned long long** out);

// Convert a char array that represents a hexadecimal number
// to long long number
void convert_charhex_to_64(char* s, unsigned long long* out);

#endif
